sudo certbot certonly \
  --agree-tos \
  --email $1 \
  --preferred-challenges=dns \
  -d $2 \
  -d *.$2 \
  --server https://acme-v02.api.letsencrypt.org/directory