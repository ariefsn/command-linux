echo $'\n===== CPU =====\n'
./cpu.sh | grep -E "^CPU\(s\)|Model name"
echo $'\n===== Memory =====\n'
./memory.sh
echo $'\n===== Storage =====\n'
./storage.sh | grep -E "Filesystem|home|overlay|/mnt"